package jp.aegif.alfresco.sample;

import java.util.Date;

import org.alfresco.web.evaluator.BaseEvaluator;
import org.json.simple.JSONObject;
import org.springframework.extensions.surf.util.ISO8601DateFormat;

public class CustomEvaluator extends BaseEvaluator {

    @Override
    public boolean evaluate(JSONObject jsonObject) {
        String modifiedDateString = (String)((JSONObject)getProperty(jsonObject, "cm:modified")).get("iso8601");
        Date modifiedDate = ISO8601DateFormat.parse(modifiedDateString);
        Date now = new Date();
        if (now.getTime() - modifiedDate.getTime() < 24*60*60*1000) {
            return true;
        }
        return false;
    }

}
